import { useCallback } from 'react'
import {
  processPayment,
  PaymentRequestBody,
  PaymentResponseData,
} from '~services/requests/paymentService'

export const usePayment = () => {
  const makePayment = useCallback(async (paymentData: PaymentRequestBody): Promise<any> => {
    try {
      const response: PaymentResponseData = await processPayment(paymentData)
      if (response.isSuccess) {
        return response.data
      } else {
        throw new Error('Payment was unsuccessful.')
      }
    } catch (error) {
      console.error('Error making payment:', error)
      throw error
    }
  }, [])

  return { makePayment }
}
