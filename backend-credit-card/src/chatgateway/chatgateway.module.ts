import { Module } from '@nestjs/common';
import { ChatGateway } from './chatgateway.gateway';
import { ChatGatewayService } from './chatgateway.service';

@Module({
  providers: [ChatGateway, ChatGatewayService],
  exports: [ChatGatewayService],
})
export class ChatGatewayModule {}
