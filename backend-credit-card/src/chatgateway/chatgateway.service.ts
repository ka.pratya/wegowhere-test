import { Injectable } from '@nestjs/common';
import { ChatGateway } from './chatgateway.gateway';

@Injectable()
export class ChatGatewayService {
  constructor(private chatGateway: ChatGateway) {}

  sendPaymentSuccessMessage(
    username: string,
    amount: number,
    currency: string,
  ) {
    const message = `${username} has successfully made a payment of ${amount} ${currency.toUpperCase()}`;
    console.log('Emitting paymentSuccess message:', message);

    this.chatGateway.server.emit('paymentSuccess', { message });
  }
}
