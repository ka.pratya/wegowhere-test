import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { CardDetailsDto } from './dto/card-details.dto';
import { ChargeDetailsDto } from './dto/charge-details.dto';
import { PAYMENT_CONFIG } from '../config/payment-config';
import { ChatGatewayService } from '../chatgateway/chatgateway.service';

@Injectable()
export class PaymentService {
  private omise: any;

  constructor(
    private config: ConfigService,
    private chatGatewayService: ChatGatewayService,
  ) {
    this.omise = require('omise')({
      publicKey: this.config.get<string>('OMISE_PUBLIC_KEY'),
      secretKey: this.config.get<string>('OMISE_SECRET_KEY'),
    });
  }

  async createToken(cardDetails: CardDetailsDto): Promise<any> {
    return new Promise((resolve, reject) => {
      try {
        this.omise.tokens.create(
          {
            card: cardDetails,
          },
          (err, token) => {
            if (err) {
              reject({ isSuccess: false, message: err.message });
            } else {
              resolve({
                isSuccess: true,
                data: {
                  id: token.id,
                  created_at: token.created_at,
                },
              });
            }
          },
        );
      } catch (error) {
        reject({ isSuccess: false, message: error.message });
      }
    });
  }

  async createCharge(
    chargeDetails: ChargeDetailsDto,
    name: string,
  ): Promise<any> {
    const feeRate = PAYMENT_CONFIG.feeRate;
    const vatRate = PAYMENT_CONFIG.vatRate;
    const feeFlat = PAYMENT_CONFIG.feeRate;

    const totalChargeAmount = this.calculateChargeAmountForDesiredNet(
      chargeDetails.amount,
      feeRate,
      vatRate,
      feeFlat,
    );

    const omiseChargeDetails = {
      amount: totalChargeAmount,
      currency: chargeDetails.currency,
      card: chargeDetails.token,
      capture: chargeDetails.autoCapture,
    };

    return new Promise((resolve, reject) => {
      try {
        this.omise.charges.create(omiseChargeDetails, (err, charge) => {
          if (err) {
            reject({ isSuccess: false, message: err.message });
          } else {
            this.chatGatewayService.sendPaymentSuccessMessage(
              name,
              charge.amount / 100,
              charge.currency,
            );

            resolve({
              isSuccess: true,
              data: {
                object: charge.object,
                totalAmount: charge.amount / 100,
                net: charge.net / 100,
                totalFee: (charge.amount - charge.net) / 100,
              },
            });
          }
        });
      } catch (error) {
        reject({ isSuccess: false, message: error.message });
      }
    });
  }

  calculateChargeAmountForDesiredNet(
    desiredNetAmount: number,
    feeRate: number,
    vatRate: number,
    feeFlat: number = 0,
  ): number {
    const feeRateDecimal = feeRate / 100;
    const vatRateDecimal = vatRate / 100;

    let estimatedChargeAmount = desiredNetAmount;
    let calculatedNetAmount = 0;

    while (calculatedNetAmount < desiredNetAmount) {
      const feeWithoutVat = estimatedChargeAmount * feeRateDecimal + feeFlat;
      const vatOnFee = feeWithoutVat * vatRateDecimal;
      const totalFee = feeWithoutVat + vatOnFee;

      calculatedNetAmount = estimatedChargeAmount - totalFee;

      if (calculatedNetAmount < desiredNetAmount) {
        estimatedChargeAmount++;
      }
    }

    return estimatedChargeAmount;
  }
}
