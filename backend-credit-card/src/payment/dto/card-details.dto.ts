import { IsString, IsInt, Min, Max, Length } from 'class-validator';

export class CardDetailsDto {
  @IsString()
  name: string;

  @IsString()
  @Length(16, 16)
  number: string;

  @IsInt()
  @Min(1)
  @Max(12)
  expiration_month: number;

  @IsInt()
  @Min(new Date().getFullYear())
  expiration_year: number;

  @IsString()
  city: string;

  @IsString()
  postal_code: string;

  @IsString()
  @Length(3, 4)
  security_code: string;
}
