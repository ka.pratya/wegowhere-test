import {
  IsInt,
  IsString,
  IsOptional,
  Min,
  Max,
  IsNumber,
  Length,
  IsBoolean,
} from 'class-validator';

export class ProcessPaymentDto {
  @IsString()
  name: string;

  @IsString()
  @Length(16, 16)
  number: string;

  @IsInt()
  @Min(1)
  @Max(12)
  expiration_month: number;

  @IsInt()
  @Min(new Date().getFullYear())
  expiration_year: number;

  @IsString()
  city: string;

  @IsString()
  postal_code: string;

  @IsString()
  @Length(3, 4)
  security_code: string;

  @IsNumber()
  @Min(20)
  amount: number;

  @IsString()
  currency: string;

  @IsOptional()
  @IsString()
  token?: string;

  @IsOptional()
  @IsBoolean()
  autoCapture?: boolean;
}
