import {
  IsNumber,
  IsString,
  IsOptional,
  IsBoolean,
  Min,
} from 'class-validator';

export class ChargeDetailsDto {
  @IsNumber()
  @Min(21)
  amount: number;

  @IsString()
  currency: string;

  @IsString()
  token: string;

  @IsOptional()
  @IsBoolean()
  autoCapture?: boolean = true;
}
