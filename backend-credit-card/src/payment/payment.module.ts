import { Module } from '@nestjs/common';
import { PaymentController } from './payment.controller';
import { PaymentService } from './payment.service';
import { ChatGatewayModule } from '../chatgateway/chatgateway.module';

@Module({
  imports: [ChatGatewayModule],
  controllers: [PaymentController],
  providers: [PaymentService],
})
export class PaymentModule {}
