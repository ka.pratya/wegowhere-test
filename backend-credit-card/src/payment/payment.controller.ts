import { Body, Controller, Post } from '@nestjs/common';
import { PaymentService } from './payment.service';
import { ProcessPaymentDto } from './dto/process-payment.dto';
import { ChargeDetailsDto } from './dto/charge-details.dto';
import { HttpException, HttpStatus } from '@nestjs/common';

@Controller('payment')
export class PaymentController {
  constructor(private paymentService: PaymentService) {}

  @Post('processPayment')
  async processPayment(@Body() paymentDetails: ProcessPaymentDto) {
    try {
      const tokenResponse =
        await this.paymentService.createToken(paymentDetails);
      if (!tokenResponse.isSuccess) {
        return { isSuccess: false, message: tokenResponse.message };
      }

      const currency = paymentDetails.currency.toLowerCase();

      if (currency !== 'thb') {
        throw new HttpException(
          {
            isSuccess: false,
            message: "Error: Only 'THB' currency is supported.",
          },
          HttpStatus.BAD_REQUEST,
        );
      }

      const chargeDetails = new ChargeDetailsDto();
      chargeDetails.amount = paymentDetails.amount * 100;
      chargeDetails.currency = paymentDetails.currency;
      chargeDetails.token = tokenResponse.data.id;
      chargeDetails.autoCapture = paymentDetails.autoCapture;

      const chargeResponse = await this.paymentService.createCharge(
        chargeDetails,
        paymentDetails.name,
      );
      return chargeResponse;
    } catch (error) {
      return {
        isSuccess: false,
        message:
          error.message || 'An error occurred during the payment process.',
      };
    }
  }
}
