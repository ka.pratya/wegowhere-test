declare module 'omise' {
  export interface OmiseTokens {
    create: (
      cardDetails: any,
      callback: (error: any, token: any) => void,
    ) => void;
  }

  export interface OmiseCharges {
    create: (
      chargeDetails: any,
      callback: (error: any, charge: any) => void,
    ) => void;
  }

  export interface OmiseCustomers {
    create: (
      customerDetails: any,
      callback: (error: any, customer: any) => void,
    ) => void;
  }

  export interface Omise {
    tokens: OmiseTokens;
    charges: OmiseCharges;
    customers: OmiseCustomers;
  }

  const omise: (config: { publicKey: string; secretKey: string }) => Omise;
  export = omise;
}
